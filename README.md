# API para controle de estacionamento - V1.0

### API desenvolvida em .Net Core 2.1 + Base de dados MySql.

## Como utilizar a API

> 1- Baixe o projeto e execute o script contido em ApiEstacioamento\Scripts\scripts_base_app_estacionamento.sql <br>

> 2 - Configurar o appsettings.json com as informações da base de dados <br>

> 3 - Após execução acessar em https://localhost:44384/api/estacionamento


