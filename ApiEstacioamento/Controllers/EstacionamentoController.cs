﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ApiEstacioamento.Model;
using ApiEstacioamento.Model.Request;
using ApiEstacioamento.Model.Response;
using ApiEstacioamento.Service;
using AutoMapper;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ApiEstacioamento.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("MyPolicy")]
    [Produces("application/json")]
    [ApiController]
    public class EstacionamentoController : Controller
    {
        IMapper _mapper;

        private IEstacionamentoService _service;

        public EstacionamentoController(IEstacionamentoService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET
        [HttpGet]
        public IActionResult getCarros()
        {
            List<Veiculo> carros = _service.ListarVeiculos();
            if (carros.Count > 0)
            {
                return Ok(_service.ListarVeiculos());
            }
            else
            {
                return BadRequest("Nenhum veiculo cadastrado!");
            }
        }

        // GET
        [HttpGet("{id}")]
        public IActionResult getVeiculo(long id)
        {
            Veiculo veiculo = _service.GetVeiculoPorId(id);
            if (veiculo != null)
            {
                return Ok(_mapper.Map<VeiculoResponse>(veiculo));
            }
            else
            {
                return BadRequest("Nenhum veiculo cadastrado com o ID: " + id);
            }
        }

        // GET
        [HttpGet("placa/{placa}")]
        public IActionResult getVeiculoPorPlaca(string placa)
        {
            Veiculo veiculo = _service.GetVeiculoPorPlaca(placa);

            if (veiculo != null)
            {
                return Ok(_mapper.Map<VeiculoResponse>(veiculo));
            }
            else
            {
                return BadRequest("Nenhum veiculo cadastrado com a placa: " + placa);
            }
        }


        [HttpPost]
        public IActionResult EntradaDeVeiculo([FromBody] VeiculoRequest request)
        {
            return Ok(_service.EntradaDeVeiculo(_mapper.Map<Veiculo>(request)));
        }


        [HttpPut]
        public IActionResult SaidaDeVeiculo([FromBody] VeiculoSaidaRequest request)
        {
            return Ok(_service.SaidaDeVeiculo(_mapper.Map<Veiculo>(request)));
        }

        [HttpGet("precos/{data}")]
        public IActionResult ValoresEstacionamento(string data)
        {
            List<PrecoVigencia> precos = _service.ValoresEstacionamento(DateTime.Parse(data));
            if (precos.Count > 0)
            {
                return Ok(precos);
            }
            else
            {
                return BadRequest("Nenhum preço cadastrado!");
            }
        }

        [HttpGet("precos/{data}/{tipo}")]
        public IActionResult ValoresEstacionamentoPorTipo(string data, long tipo)
        {
            List<PrecoVigencia> precos = _service.ValoresEstacionamentoPorTipo(DateTime.Parse(data), tipo);
            if (precos.Count > 0)
            {
                return Ok(precos);
            }
            else
            {
                return BadRequest("Nenhum preço cadastrado!");
            }
        }
    }
}
