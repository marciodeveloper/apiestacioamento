﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiEstacioamento.Model.Context;
using ApiEstacioamento.Service;
using ApiEstacioamento.Service.ServiceImpl;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySql.Data.EntityFrameworkCore.Extensions;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using ApiEstacioamento.Model.Request;
using ApiEstacioamento.Model;
using ApiEstacioamento.Repository;

namespace ApiEstacioamento
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //Configurando Base de dados MySql
            var connection = Configuration["MySqlConnection:MySqlConnectionString"];
            services.AddDbContext<MySQLContext>(options => options.UseMySQL(connection));

            //CORS
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //Dependencias
            services.AddScoped<IEstacionamentoService, EstacionamentoService>();
            services.AddScoped<IEstacionamentoRepository, EstacionamentoRepository>();
            services.AddScoped<IPrecoVigenciaRepository, PrecoVigenciaRepository>();

            //Config Automapper
            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<VeiculoRequest, Model.Veiculo>();
                cfg.CreateMap<Veiculo, Model.Request.VeiculoRequest>();
                cfg.CreateMap<Veiculo, Model.Response.VeiculoResponse>();
                cfg.CreateMap<VeiculoSaidaRequest, Model.Veiculo>();
                cfg.CreateMap<Veiculo, Model.Request.VeiculoSaidaRequest>();


            });
            IMapper mapper = config.CreateMapper();
            services.AddSingleton(mapper);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors("MyPolicy");
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
