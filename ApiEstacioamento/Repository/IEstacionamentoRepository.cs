﻿using ApiEstacioamento.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstacioamento.Service
{
    public interface IEstacionamentoRepository
    {
        List<Veiculo> ListarVeiculos();
        Veiculo GetVeiculoPorId(long idVeiculo);
        Veiculo GetVeiculoPorPlaca(string placaVeiculo);
        Veiculo EntradaDeVeiculo(Veiculo veiculo);
        Veiculo SaidaDeVeiculo(Veiculo veiculo);
    }
}
