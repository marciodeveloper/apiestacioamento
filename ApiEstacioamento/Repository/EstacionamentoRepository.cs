﻿using ApiEstacioamento.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiEstacioamento.Model;

namespace ApiEstacioamento.Service.ServiceImpl
{
    public class EstacionamentoRepository : IEstacionamentoRepository
    {
        private MySQLContext _context;

        public EstacionamentoRepository(MySQLContext context)
        {
            _context = context;
        }

        public List<Veiculo> ListarVeiculos()
        {
            try
            {
                return _context.Veiculo.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }

        public Veiculo GetVeiculoPorId(long idVeiculo)
        {
            var result = new Veiculo();
            try
            {
                result = _context.Veiculo.SingleOrDefault(v => v.Id_veiculo.Equals(idVeiculo));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }

        public Veiculo GetVeiculoPorPlaca(string placaVeiculo)
        {
            var result = new Veiculo();
            try
            {
                result = _context.Veiculo.SingleOrDefault(v => v.Placa.Equals(placaVeiculo));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }

        public Veiculo EntradaDeVeiculo(Veiculo veiculo)
        {
            try
            {
                _context.Add(veiculo);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return veiculo;
        }

        public Veiculo SaidaDeVeiculo(Veiculo veiculo)
        {
            var result = new Veiculo();
            try
            {
                result = _context.Veiculo.SingleOrDefault(v => v.Placa.Equals(veiculo.Placa));

                result.Data_saida = new DateTime();
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }

    }
}
