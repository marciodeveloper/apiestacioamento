﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiEstacioamento.Model;
using ApiEstacioamento.Model.Context;

namespace ApiEstacioamento.Repository
{
    public class PrecoVigenciaRepository : IPrecoVigenciaRepository
    {

        private MySQLContext _context;

        public PrecoVigenciaRepository(MySQLContext context)
        {
            _context = context;
        }


        public List<PrecoVigencia> ValoresEstacionamento(DateTime data)
        {
            try
            {
                return _context.PrecoVigencia.Where(p => 
                Convert.ToDateTime(p.Data_inicio) <= Convert.ToDateTime(data) 
                && Convert.ToDateTime(p.Data_fim) >= Convert.ToDateTime(data)
                ).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PrecoVigencia> ValoresEstacionamentoPorTipo(DateTime data, long tipo)
        {
            try
            {
                return _context.PrecoVigencia.Where(p => p.Tipo.Equals(tipo) 
                && Convert.ToDateTime(p.Data_inicio) <= Convert.ToDateTime(data)
                && Convert.ToDateTime(p.Data_fim) >= Convert.ToDateTime(data)
                ).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
