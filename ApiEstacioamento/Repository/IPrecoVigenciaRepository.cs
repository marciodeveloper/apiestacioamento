﻿using ApiEstacioamento.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstacioamento.Repository
{
    public interface IPrecoVigenciaRepository
    {
        List<PrecoVigencia> ValoresEstacionamento(DateTime data);
        List<PrecoVigencia> ValoresEstacionamentoPorTipo(DateTime data, long tipo);
    }
}
