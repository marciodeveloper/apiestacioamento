﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiEstacioamento.Model
{
    [Table("veiculo")]
    public class Veiculo
    {
        [Key]
        public long Id_veiculo { get; set; }
        [Required(ErrorMessage = "Campo placa é obrigatorio.")]
        [StringLength(7, ErrorMessage = "Tamanho maximo da placa é de 7")]
        public string Placa { get; set; }
        [Required(ErrorMessage = "Campo data entrada é obrigatorio.")]
        public DateTime Data_entrada { get; set; }
        public DateTime? Data_saida { get; set; }
    }
}