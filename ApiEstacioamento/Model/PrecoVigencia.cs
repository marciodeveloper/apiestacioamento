﻿using ApiEstacioamento.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstacioamento.Model
{
    [Table("preco_vigencia")]
    public class PrecoVigencia
    {
        [Key]
        public long Id_preco_vigencia { get; set; }
        [Required(ErrorMessage = "Campo data inicio é obrigatorio.")]
        public DateTime Data_inicio { get; set; }
        [Required(ErrorMessage = "Campo data fim é obrigatorio.")]
        public DateTime Data_fim { get; set; }
        [Required(ErrorMessage = "Campo valor é obrigatorio.")]
        public decimal Valor { get; set; }
        [Required(ErrorMessage = "Campo tipo é obrigatorio.")]
        public long Tipo { get; set; }
    }
}
