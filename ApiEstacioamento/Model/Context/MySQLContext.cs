﻿using ApiEstacioamento.Service;
using Microsoft.EntityFrameworkCore;

namespace ApiEstacioamento.Model.Context
{
    public class MySQLContext : DbContext
    {
        public MySQLContext() { }

        public MySQLContext(DbContextOptions<MySQLContext> options) : base(options) { }

        public DbSet<Veiculo> Veiculo { get; set; }

        public DbSet<PrecoVigencia> PrecoVigencia { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }

    }
}
