﻿using ApiEstacioamento.Enums;
using ApiEstacioamento.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstacioamento.Model
{
    [Table("pagamento")]
    public class Pagamento
    {
        [Key]
        public long Id { get; set; }
        public Veiculo veiculo { get; set; }
        public PrecoVigencia preco { get; set; }
        public decimal valor { get; set; }
        public StatusPagamento status { get; set; }

    }
}
