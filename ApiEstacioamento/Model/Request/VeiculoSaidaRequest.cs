﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstacioamento.Model.Request
{
    public class VeiculoSaidaRequest
    {
        public long Id_veiculo { get; set; }
        public string Placa { get; set; }
        public DateTime Data_entrada { get; set; }
        public DateTime? Data_saida { get; set; }
    }
}
