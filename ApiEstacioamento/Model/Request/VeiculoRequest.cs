﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstacioamento.Model.Request
{
    public class VeiculoRequest
    {
        public string Placa { get; set; }
        public DateTime Data_entrada { get; set; }
    }
}
