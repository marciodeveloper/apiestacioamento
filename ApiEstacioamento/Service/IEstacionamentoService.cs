﻿using ApiEstacioamento.Model;
using ApiEstacioamento.Model.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstacioamento.Service
{
    public interface IEstacionamentoService
    {
        List<Veiculo> ListarVeiculos();
        Veiculo GetVeiculoPorId(long idVeiculo);
        Veiculo GetVeiculoPorPlaca(string placaVeiculo);
        Veiculo EntradaDeVeiculo(Veiculo veiculo);
        Veiculo SaidaDeVeiculo(Veiculo veiculo);
        List<PrecoVigencia> ValoresEstacionamento(DateTime data);
        List<PrecoVigencia> ValoresEstacionamentoPorTipo(DateTime data, long tipo);
    }
}
