﻿using ApiEstacioamento.Model;
using ApiEstacioamento.Model.Context;
using ApiEstacioamento.Model.Request;
using ApiEstacioamento.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstacioamento.Service.ServiceImpl
{
    public class EstacionamentoService : IEstacionamentoService
    {
        private IEstacionamentoRepository _repository;
        private IPrecoVigenciaRepository _respositoryPreco;

        public EstacionamentoService(IEstacionamentoRepository repository, IPrecoVigenciaRepository respositoryPreco)
        {
            _repository = repository;
            _respositoryPreco = respositoryPreco;
        }

        public List<Veiculo> ListarVeiculos()
        {
            try
            {
                return _repository.ListarVeiculos();
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }

        public Veiculo GetVeiculoPorId(long idVeiculo)
        {
            var result = new Veiculo();
            try
            {
                result = _repository.GetVeiculoPorId(idVeiculo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }

        public Veiculo GetVeiculoPorPlaca(string placaVeiculo)
        {
            var result = new Veiculo();
            try
            {
                result = _repository.GetVeiculoPorPlaca(placaVeiculo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }

        public Veiculo EntradaDeVeiculo(Veiculo veiculo)
        {
            try
            {
                Veiculo response =  _repository.EntradaDeVeiculo(veiculo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return veiculo;
        }

        public Veiculo SaidaDeVeiculo(Veiculo veiculo)
        {
            var result = new Veiculo();
            try
            {
                result = _repository.GetVeiculoPorPlaca(veiculo.Placa);
                result.Data_saida = new DateTime();
                _repository.SaidaDeVeiculo(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }

        public List<PrecoVigencia> ValoresEstacionamento(DateTime data)
        {
            try
            {
                return _respositoryPreco.ValoresEstacionamento(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PrecoVigencia> ValoresEstacionamentoPorTipo(DateTime data, long tipo)
        {
            try
            {
                return _respositoryPreco.ValoresEstacionamentoPorTipo(data, tipo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
