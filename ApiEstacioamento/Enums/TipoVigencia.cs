﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstacioamento.Enums
{
    public enum TipoVigencia
    {
        INICIAL,
        ADICIONAL
    }
}
