CREATE SCHEMA `estacionamento_db` ;

CREATE TABLE `estacionamento_db`.`preco_vigencia` (
  `id_preco_vigencia` INT NOT NULL AUTO_INCREMENT,
  `data_inicio` DATE NOT NULL,
  `data_fim` DATE NULL,
  `valor` INT NULL,
  PRIMARY KEY (`id_preco_vigencia`));


CREATE TABLE `estacionamento_db`.`tipo_preco_vigencia` (
  `id_tipo_preco_vigencia` INT NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_tipo_preco_vigencia`));


ALTER TABLE `estacionamento_db`.`preco_vigencia` 
ADD COLUMN `tipo` INT NOT NULL AFTER `valor`,
ADD INDEX `tipo_idx` (`tipo` ASC) VISIBLE;

ALTER TABLE `estacionamento_db`.`preco_vigencia` 
ADD CONSTRAINT `tipo`
  FOREIGN KEY (`tipo`)
  REFERENCES `estacionamento_db`.`tipo_preco_vigencia` (`id_tipo_preco_vigencia`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

CREATE TABLE `estacionamento_db`.`status_pagamento` (
  `id_status_pagamento` INT NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_status_pagamento`));

 CREATE TABLE `estacionamento_db`.`veiculo` (
  `id_veiculo` INT NOT NULL AUTO_INCREMENT,
  `placa` VARCHAR(45) NOT NULL,
  `data_entrada` DATETIME NOT NULL,
  `data_saida` DATETIME NULL,
  PRIMARY KEY (`id_veiculo`));
 

CREATE TABLE `estacionamento_db`.`pagamento` (
  `id_pagamento` INT NOT NULL AUTO_INCREMENT,
  `veiculo` INT NOT NULL,
  `preco` INT NOT NULL,
  `valor` INT NOT NULL,
  `status` INT NOT NULL,
  PRIMARY KEY (`id_pagamento`),
  INDEX fk_veiculo_idx (`veiculo`),
  INDEX fk_preco_idx (`preco`),
  INDEX fk_status_idx (`status`),
  CONSTRAINT `fk_veiculo`
    FOREIGN KEY (`veiculo`)
    REFERENCES `estacionamento_db`.`veiculo` (`id_veiculo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_preco`
    FOREIGN KEY (`preco`)
    REFERENCES `estacionamento_db`.`preco_vigencia` (`id_preco_vigencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_status`
    FOREIGN KEY (`status`)
    REFERENCES `estacionamento_db`.`status_pagamento` (`id_status_pagamento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-------------------------- INSERTS ---------------------------------------------

INSERT INTO `estacionamento_db`.`tipo_preco_vigencia`
(`descricao`)
VALUES
('inicial');
;

INSERT INTO `estacionamento_db`.`tipo_preco_vigencia`
(`descricao`)
VALUES
('adicionais');

-------------------------------------------------

  INSERT INTO `estacionamento_db`.`preco_vigencia`
(data_inicio,
data_fim,
valor,
tipo)
VALUES
(CURDATE(),
DATE_ADD(CURDATE(), INTERVAL 10 day),
50,
1);

INSERT INTO `estacionamento_db`.`preco_vigencia`
(data_inicio,
data_fim,
valor,
tipo)
VALUES
(CURDATE(),
DATE_ADD(CURDATE(), INTERVAL 10 DAY),
25,
2);

------------------------------------------------------------------

INSERT INTO `estacionamento_db`.`veiculo`
(`placa`,
`data_entrada`,
`data_saida`)
VALUES
('MAR-1987',
CURDATE(),
null);

INSERT INTO `estacionamento_db`.`veiculo`
(`placa`,
`data_entrada`,
`data_saida`)
VALUES
('JOP-4587',
CURDATE(),
null);
